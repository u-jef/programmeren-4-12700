﻿<footer>
    <p class="copy">concept & design - Entreprise de modes et de manières modernes 
        2012-<?php echo date('Y');?></p>

    <div class="vcard">
        <h3>Contact</h3>

        <p class="fn org">Modern Ways</p>

        <div class="adr">
            <div class="street-address">Braziliëstraat 38</div>
            <div class="postal-code">2000</div>
            <div class="locality">Antwerpen</div>
            <div class="country-name">België</div>
        </div>
    </div>
</footer>