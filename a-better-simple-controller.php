<?php
// we gaan ervan uit dat er op geen knop geklikt is
$message = "Nog geen keuze gemaakt.";
if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case 'add-to-cart' :
            $message = "Je wilt iets in de kar stoppen.";
            break;
        case 'empty-cart' :
            $message = "Je wilt de winkelkar leegmaken.";
            break;
        case 'delete-article' :
            $message = "Je wilt een artikel uit de winkelkar halen.";
            break;
        default :
            // $message = "Nog geen keuze gemaakt.";
    }
} 
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>">
            <button type="submit" name="action" value="add-to-cart">In winkelwagentje</button>
            <button type="submit" name="action" value="empty-cart">Winkelwagentje leegmaken</button>
            <button type="submit" name="action" value="delete-article">Artikel uit kar verwijderen</button>
        </form>
        <p><?php echo $message;?></p>
    </body>
</html>
