<?php
namespace ModernWays\WebShop\Dal;

class UnitBase
{
    private $modelState;

    public function __construct($modelState) {
        $this->modelState = $modelState;
    }
    
    public function getModelState() {
        return $this->modelState;
    }
    public function ReadAll()
    {
        $provider = new \ModernWays\WebShop\Dal\Provider($this->modelState);
        $provider->open();
        $queryString = 'call UnitBaseReadAllOrderByName';
        $preparedStatement = $provider->getPdo()->prepare($queryString);
        $preparedStatement->execute();
        // fetchAll retourneert rijen als dictionaries
        $result = $preparedStatement->fetchAll();
        return $result;
    }

    public function Create($code, $name, $description, $shippingCostMultiplier)
    {
        $provider = new \ModernWays\WebShop\Dal\Provider($this->modelState);
        $provider->open();
        $queryString = 'CALL UnitBaseCreate(:pName, :pCode, :pDescription,:pShippingCostMultiplier, :pInsertedBy)';
        
        $preparedStatement = $provider->getPdo()->prepare($queryString);
        // so we cannot use bindParam that requires a variable by value
		// if you want to use a variable, use then bindParam
        $preparedStatement->bindParam(':pName', $name, \PDO::PARAM_STR);
        $preparedStatement->bindParam(':pCode', $code, \PDO::PARAM_STR);
        $preparedStatement->bindParam(':pDescription', $description, \PDO::PARAM_STR);
        $preparedStatement->bindParam(':pShippingCostMultiplier',
                $shippingCostMultiplier);
        $preparedStatement->bindValue(':pInsertedBy', 'JI', \PDO::PARAM_STR);
        $preparedStatement->execute();
        echo $queryString;
        return $preparedStatement->rowCount();
    }
}