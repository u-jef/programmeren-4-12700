<?php
// Simple basic unit test Provider
require('Connection.php');
require('Provider.php');
require('UnitBase.php');

$modelState = array();
$unitBase = new \ModernWays\WebShop\Dal\UnitBase($modelState);
$result = $unitBase->Create('K', 'Kilo', 'Weegeenheid', 3);
$unitBases = $unitBase->ReadAll();
print_r($unitBases);
$modelState = $unitBase->getModelState();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>Test UnitBase</title>
</head>
<body>
<div>
    <label>Aantal rijen toegevoegd: </label>
    <label><?php echo $result; ?></label>
</div>
<div>
    <table>
        <tr>
            <th>Naam</th>
            <th>Code</th>
            <th>Beschrijving</th>
            <th>Verzendingskostfactor</th>
            <th>Ge�nserted door</th>
            <th>op</th>
        </tr>
        <?php foreach ($unitBases as $item) { ?>
            <tr>
                <td><?php echo $item['Name'];?></td>
                <td><?php echo $item['Code'];?></td>
                <td><?php echo $item['Description'];?></td>
                <td><?php echo $item['ShippingCostMultiplier'];?></td>
                <td><?php echo $item['InsertedBy'];?></td>
                <td><?php echo $item['InsertedOn'];?></td>
            </tr>
        <?php } ?>
    </table>
</div>
</body>
</html>
