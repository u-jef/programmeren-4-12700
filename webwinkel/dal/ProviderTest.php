<?php
    // Simple basic unit test Provider
    require ('Connection.php');
    require ('Provider.php');
    $modelState = array();
    $provider = new ModernWays\WebShop\Dal\Provider($modelState);
    $provider->open();
    $modelState = $provider->getModelState();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Webwinkel</title>
    </head>
    <body>
        <div class="feedback">
            <?php echo $modelState['Connection'];?>
        </div>
    </body>
</html>
