<?php
    // de connectie klasse wordt in alle app's
    // van mijn bedrijf gebruikt
namespace ModernWays\Helpers\Dal;
    
// zelfde naam als bestand om autoload te kunnen gebruiken
class Connection
{
    protected $hostName;
    protected $databaseName;
    protected $userName;
    protected $password;
    protected $modelState;
    protected $pdo;

    public function getPdo() {
        return $this->pdo;
    }

    private function setPdo($value) {
        $this->pdo = $value;
    }

    public function getModelState() {
        return $this->modelState;
    }

    /**
    * @return mixed
    */
    public function getDatabaseName() {
        return $this->databaseName;
    }
    
    /**
        * @param mixed $databaseName
        */
    public function setDatabaseName($databaseName) {
        $this->databaseName = $databaseName;
    }
    
    /**
        * @return mixed
        */
    public function getUserName() {
        return $this->userName;
    }
    
    /**
        * @param mixed $userName
        */
    public function setUserName($userName) {
        $this->userName = $userName;
    }
    
    /**
        * @return mixed
        */
    public function getHostName() {
        return $this->hostName;
    }
    
    /**
        * @param mixed $hostName
        */
    public function setHostName($hostName) {
        $this->hostName = $hostName;
    }
    
    /**
        * @return mixed
        */
    public function getPassword() {
        return $this->password;
    }
    
    /**
        * @param mixed $password
        */
    public function setPassword($password) {
        $this->password = $password;
    }

    public function isConnected() {
        return (!is_null($this->getPdo()));
    }
    
    public function __construct($modelState) {
        $this->modelState = $modelState;
    }

    public function open() {
        
        $this->setPdo(null);
        try {
            $connectionString = "mysql:host={$this->hostName};dbname={$this->databaseName}";
            // echo $connectionString;
            $this->setPdo(new \PDO($connectionString, $this->userName, $this->password));
            $this->modelState['Connection'] = "Connected to {$this->databaseName} at {$this->hostName} successfully.";
        } catch (\PDOException $pe) {
            $this->modelState['Connection'] = "Connected to {$this->databaseName} at {$this->hostName} failed.";
        }
        // print_r($this->modelState);
        return ($this->isConnected());
    }

    public function close() {
        $this->setPdo(NULL);
        $this->modelState['Connection'] = "Disconnected to {$this->databaseName} at {$this->hostName}.";
   }
}
