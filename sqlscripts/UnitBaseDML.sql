use JefInghelbrecht;

drop procedure if exists UnitBaseCreate;
create procedure UnitBaseCreate (
	pName nvarchar(255),
    pCode varchar(2),
    pDescription nvarchar(1024),
    pShippingCostMultiplier float,
    pInsertedBy nvarchar(255)
)
insert into UnitBase (
	`Name`,
	`Code`,
	Description,
	ShippingCostMultiplier,
	InsertedOn,
	InsertedBy
)
values (
	pName,
	pCode,
	pDescription,
	pShippingCostMultiplier,
	Now(),
	pInsertedBy
);

drop procedure if exists UnitBaseReadAllOrderByName;
create procedure UnitBaseReadAllOrderByName ()
select `Name`, `Code`, Description, 
	ShippingCostMultiplier, InsertedBy, InsertedOn,
    UpdatedBy, UpdatedOn, Id
    from UnitBase
    order by `Name`;

-- call UnitBaseCreate('Meter', 'M', 'Meeteenheid', 3.5, 'JI');
call UnitBaseReadAllOrderByName;

show procedure status;