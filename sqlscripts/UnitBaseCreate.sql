-- modernways.be
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE UnitBase
-- Created on Monday 30th of November 2015 08:13:02 PM
-- 
USE `JefInghelbrecht`;
DROP TABLE IF EXISTS `UnitBase`;
CREATE TABLE `UnitBase` (
	`Name` NVARCHAR (255) NOT NULL,
	`Description` NVARCHAR (1024) NULL,
	`ShippingCostMultiplier` FLOAT NULL,
	`Code` NVARCHAR (2) NOT NULL,
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`InsertedBy` NVARCHAR (255) NULL,
	`InsertedOn` TIMESTAMP NULL,
	`UpdatedBy` NVARCHAR (255) NULL,
	`UpdatedOn` TIMESTAMP NULL,
	CONSTRAINT uc_UnitBase_Name UNIQUE (Name),
	CONSTRAINT uc_UnitBase_Code UNIQUE (Code));

