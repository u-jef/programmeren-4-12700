<?php
    $bestanden = array (
        array ('address', 'csv', 250),
        array ('info', 'php', 250),
        array ('leren-werken-met-array', 'php', 250),
        array ('leren-werken-met-form', 'php', 250),
        array ('tutorial', 'php', 214)
    );
    // print_r($bestanden);
    
    $personen = array(
        'Jan' => array ('blauw', 'Clio', 90),
        'An' => array ('blauw', 'Mercedes', 60),
        'Kamiel' => array ('groen', 'Volvo', 90),
        'Greet' => array ('bruin', 'Peugeot', 90)
    );
    
    $vrienden = array(
        'Jan' => 'Clio',
        'An' => 'Mercedes',
        'Kamiel' =>'Volvo',
        'Greet' => 'Peugeot'
    );
    print_r($personen);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>

        <table>
            <thead>
            <tr>
                <td>Naam</td>
                <td>Extensie</td>
                <td>Grootte</td>
            </tr>
            </thead>
            <tbody>
                <?php foreach ($bestanden as $bestand) {?>
            <tr>
                <td><?php echo $bestand[0]; ?></td>
                <td><?php echo $bestand[1]; ?></td>
                <td><?php echo $bestand[2]; ?></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>

        <table>
            <thead>
            <tr>
                <td>Naam</td>
                <td>Auto</td>
            </tr>
            </thead>
            <tbody>
                <?php foreach ($vrienden as $vriend => $auto) {?>
            <tr>
                <td><?php echo $vriend; ?></td>
                <td><?php echo $auto; ?></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
        <?php include 'footer.php'; ?>
    </body>
</html>
