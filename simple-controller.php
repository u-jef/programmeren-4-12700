<?php
    if (isset($_POST['add-to-cart'])) {
        $message = "Je wilt iets in de kar stoppen.";
    } elseif (isset($_POST['empty-cart'])) {
        $message = "Je wilt de winkelkar leegmaken.";
    } elseif (isset($_POST['delete-article'])) {
        $message = "Je wilt een artikel uit de winkelkar halen.";
    } else {
        $message = "Je wilt niets.";
    }
    
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>">
            <input type="submit" name="add-to-cart" value="In winkelwagentje">
            <input type="submit" name="empty-cart" value="Winkelwagentje leegmaken">
            <input type="submit" name="delete-article" value="In winkelwagentje">
        </form>
        <p><?php echo $message;?></p>
    </body>
</html>
