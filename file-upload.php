<?php
    $messages = array();
    // print_r($_FILES);
    $targetDir = "uploads/";
    // check als map bestaat
    if (!is_dir($targetDir)) {
    mkdir($targetDir);
    }

    if(isset($_POST["submit"])) {
        $baseName = basename($_FILES["fileToUpload"]["name"]);
        $targetFile = $targetDir . $baseName;
        $messages["target file"] = $targetFile;
        $uploadOk = 1;
        $imageFileType = pathinfo($targetFile,PATHINFO_EXTENSION);
        $messages["file extension"] = $imageFileType;
    
        // Check if image file is a actual image or fake image en
        // haal het mime type, hoogte en breedte van de image op
        $imageInfo = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($imageInfo !== false) {
            $messages["mime type"] = 
                "$baseName is een {$imageInfo["mime"]} bestand.";
                $uploadOk = 1;
        } else {
            $messages["feedback"] = "$baseName is geen afbeeldingbestand.";
            $uploadOk = 0;
        }
    
        // Check if file already exists
        if (file_exists($targetFile)) {
            $messages["feedback"] = "Sorry, $baseName bestaat al.";
            $uploadOk = 0;
        }
    
        // Limit file size
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            $messages["feedback"] = "Sorry, $baseName is te groot.";
            $uploadOk = 0;
        }
    
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            $messages["feedback"] = "Sorry, alleen JPG, JPEG, PNG & GIF bestanden zijn toegelaten.";
            $uploadOk = 0;
        }
    
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
        $messages["feedback"] = "$baseName is niet geüploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $targetFile)) {
                $messages["feedback"] = "$baseName is geüploaded.";
            } else {
                $messages["feedback"] = "Er is een fout opgetreden bij het uploaden van $baseName.";
            }
        }

        // lees alle bestanden in op alfabetische volgorde
    }
    $files = scandir($targetDir, 1)

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Mijn prentjes</title>
        <style>
            figure {
                text-align: center;
                border: solid 1px #ccc;
                border-radius: 2px;
                background: rgba(0,0,0,0.05);
                padding: 10px;
                margin: 10px 20px;
                display: inline-block;
            }

            figure > figcaption {
                text-align: center;
                display: block; /* For IE8 */
            }
            
            img {
                width:  150px;
            }
            
            img:hover {
                width: 450px;
            }
            
        </style>
    </head>
    <body>
        <article>
            <div id="controlPanel">
                <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>" method="post"
                       enctype="multipart/form-data">
                    <label for="fileToUpload">Kies een bestand</label>
                    <input type="file" name="fileToUpload" id="fileToUpload">
                    <input type="submit" value="Upload afbeelding" name="submit">
                </form>
            </div>
            <section>
            <?php
                foreach ($files as $file) {
                    if (!($file == '.' || $file == '..')) {
            ?>
                        <figure>
                            <img src="uploads/<?php echo $file;?>" alt="afbeelding <?php echo $file;?>"/>
                            <figcaption style="width:  150px;"><?php echo $file; ?></figcaption>
                        </figure>
            <?php
                    }
                }
            ?>
            </section>
        </article>
        <div id="feedback" style="clear:  both;">
            <?php
                foreach ($messages as $key => $value) {
            ?>
            <p><?php echo $key;?>: <?php echo $value; ?></p>
            <?php
                }
            ?>

        </div>
    </body>
</html>
