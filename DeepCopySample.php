<?php
/**
 * Created by PhpStorm.
 * User: jefin
 * Date: 16/12/2015
 * Time: 20:29
 */
class Persoon
{
    private $voornaam;
    private $familienaam;

    /**
     * @return mixed
     */
    public function getVoornaam()
    {
        return $this->voornaam;
    }

    /**
     * @param mixed $voornaam
     */
    public function setVoornaam($voornaam)
    {
        $this->voornaam = $voornaam;
    }

    /**
     * @return mixed
     */
    public function getFamilienaam()
    {
        return $this->familienaam;
    }

    /**
     * @param mixed $familienaam
     */
    public function setFamilienaam($familienaam)
    {
        $this->familienaam = $familienaam;
    }



    public function __construct($voornaam, $familienaam)
    {
        $this->voornaam = $voornaam;
        $this->familienaam = $familienaam;
    }
}

$personen = array();

$persoon = new Persoon('Annemie', 'Schildpad');
$personen['Eerste Minister'] = $persoon;
$persoon2 = new Persoon('Annemie', 'Schildpad');

$persoon2->setVoornaam('Walter');
$persoon2->setFamilienaam('den Boer');
$personen['Tweede Minister'] = $persoon2;
$persoon->setVoornaam('Jos');
$persoon->setFamilienaam('den Slimme');
$personen['Derde Minister'] = $persoon;

print_r($personen);
