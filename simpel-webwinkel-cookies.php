<?php
if (isset($_GET['movie'])) {
    $length = count($_COOKIE['cart']);
    setcookie("cart[$length]", $_GET['movie'], time() + 86400);
    // herlaad pagina om cookies te setten
    // header('location:simpel-webwinkel.php');
}
// http://stackoverflow.com/questions/17085821/php-cookie-set-in-second-refresh-page
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Simpel webwinkel</title>
    </head>
    <body>
        <h1>Mikmak</h1>
        <form method="get" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>">
            <select name="movie" id="movie">
                <option>Hercule Poirot</option>
                <option>Inspector Linsley</option>
                <option>Sneeuwwitje en de zeven dwergen</option>
                <option>The return of Batman</option>
                <option>The avengers</option>
                <option>Downtown Abbey</option>
                <option>Miss Marple</option>
                <option>The point of no return</option>
                <option>The magic flute</option>
                <option>De vriendelijk reus</option>
            </select>
            <input type="submit" name="submit" value="In winkelwagentje" />
        </form>
        <div>
            <ol>
            <?php
                if (isset($_COOKIE['cart'])) {
                    foreach ($_COOKIE['cart'] as $movie) {
                    ?>
                        <li><?php echo $movie;?></li>
                    <?php
                    }
                }
                if (isset($_GET['movie'])) {
                ?>
                    <li><?php echo $_GET['movie'];?></li>
                <?php
                }
            ?>
            </ol>
        </div>
    </body>
</html>
