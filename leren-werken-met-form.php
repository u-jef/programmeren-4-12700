<?php
  $messages = array();
  if (isset($_POST['name'])) {
      if (empty($_POST['name'])) {
          $messages['name'] = 'Naam is een verplicht veld';
      } elseif (!preg_match("/^[a-zA-Z ]*$/",$_POST['name'])) {
          // check if name only contains letters and whitespace
          $messages['name'] = "Only letters and white space allowed"; 
      } else {
          $name = sanitizeInput($_POST['name']);
      }
      
      if (empty($_POST['email'])) {
          $messages['email'] = 'E-mail is een verplicht veld';
      } elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
          $messages['email'] = "Invalid email format";
      } else {
          $email = sanitizeInput($_POST['email']);
      }

      if (count($messages) == 0) {
        file_put_contents('address.csv', 
        $name . '|' . $email . PHP_EOL, 
            FILE_APPEND);
      }
  }


  function sanitizeInput($value) {
      return strip_tags($value);
  }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Leren Werken met het form element</title>
    </head>
    <body>
        <pre>
            <?php
                // print_r($_SERVER);
            ?>
        </pre>
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>" method="post">
            <div>
                <label>Name</label>
                <input type="text" name="name" autocomplete="off" required
                       value="<?php echo isset($_POST['name']) ? $_POST['name'] : '';?>">
                <span style="color: red;">
                <?php
                    if (isset($messages['name'])) {
                        echo $messages['name'];
                    }
                ?>
                </span>
            </div>
            <div>
                <label>E-mail</label>
                <input type="text" name="email" required
                        value="<?php echo isset($_POST['email']) ? $_POST['email'] : '';?>">
                <span style="color: red;">
                <?php
                    if (isset($messages['email'])) {
                        echo $messages['email'];
                    }
                ?>
                </span>
            </div>
            <div>
                <input type="submit" name="submit" value="Registeren">
            </div>
        </form>
        <div>
            <?php echo file_get_contents('address.csv');?>
        </div>
        <div>
            <?php
                if (count($messages) > 0) {
                    foreach ($messages as $key => $value) {
            ?>
                        <p>
                        <?php echo $value;?>
                        </p>
            <?php
                    }
                }
            ?>
        </div>
    </body>
</html>
