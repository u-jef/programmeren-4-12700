<?php
/**
 * Created by PhpStorm.
 * User: jefin
 * Date: 16/12/2015
 * Time: 19:53
 */
include('../autoload.php');
$modelHello = new PlopperDePlop\Model\Hello();
$modelHelloAgain = new PlopperDePlop\Model\HelloAgain();
$viewHello = new PlopperDePlop\View\Hello($modelHello);
$viewHelloAgain = new PlopperDePlop\View\Hello($modelHelloAgain);

?>
<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <title>MVC</title>
</head>
<body>
<p><?php $viewHello->render(); ?></p>
<p><?php $viewHelloAgain->render(); ?></p>
</body>
</html>
