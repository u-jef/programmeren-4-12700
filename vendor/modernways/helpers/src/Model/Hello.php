<?php
/**
 * Created by Jef Inghelbrecht.
 * User: jefin
 * Date: 13/12/2015
 * Time: 12:36
 */
namespace PlopperDePlop\Model;

class Hello
{
    // constructor wordt uitgevoerd met
    // het new keyword
    public function __construct()
    {

    }

    public function say() {
        return 'Hello World.';
    }
}