<?php
/**
 * Created by PhpStorm.
 * User: jefin
 * Date: 14/12/2015
 * Time: 21:36
 */
namespace PlopperDePlop\Model;

class HelloAgain
{
    // constructor wordt uitgevoerd met
    // het new keyword
    public function __construct()
    {
    }

    public function say() {
        return 'Hello again World.';
    }
}