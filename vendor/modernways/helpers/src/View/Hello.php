<?php
namespace PlopperDePlop\View;
class Hello
{
    private $model;

    public function __construct($model)
    {
        $this->model = $model;
    }

    public function render()
    {
        include('Template/Hello.php');
    }
}