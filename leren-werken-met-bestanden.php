<?php
    $personenString = file_get_contents('personen.txt');
    $personenStringArray = explode("\n", $personenString);
    // toevoegen van een element aan een lege array
    $personen = array();
    foreach ($personenStringArray as $persoon) {
            echo substr_count($persoon, '|');
        if (substr_count($persoon, '|') == 3) {
            $persoonArray = explode('|', $persoon);
            $personen[$persoonArray[0]] =  
                array ($persoonArray[1], 
                        $persoonArray[2], 
                        $persoonArray[3]
                );
        }
    }

    $personenJson = 
        json_decode(file_get_contents('personen.json'), true);

    /* $personen = array(
        'Jan' => array ('blauw', 'Clio', 90),
        'An' => array ('blauw', 'Mercedes', 60),
        'Kamiel' => array ('groen', 'Volvo', 90),
        'Greet' => array ('bruin', 'Peugeot', 90)
    ); */



    $personenString = '';
    foreach ($personen as $key => $value) {
        $personenString .= "$key;$value[0];$value[1];$value[2]\n";
    }

    // dit is de correcte manier
    foreach ($personen as $key => $value) {
        $personenString .= "$key";
        foreach ($value as $attribute) {
            $personenString .= "|$attribute";
        }
        $personenString = $personenString . PHP_EOL;
    }

    file_put_contents('personen1.txt', $personenString);
    file_put_contents('personen.json', json_encode($personenJson));

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Leren werken met bestanden</title>
    </head>
    <body>
        
    </body>
</html>
